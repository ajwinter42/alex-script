function getOne(query) {
    const element = this.querySelector(query)
    if (!(element instanceof HTMLElement)) {
        console.error('unable to locate element', {
            queryContainer: this,
            query,
            result: element,
        })
        return document.createElement('i')
    }
    return element
}

function getAll(query) {
    return [...this.querySelectorAll(query)]
}

export function alexScript() {
    Document.prototype.ready = function (callback) {
        this.addEventListener('DOMContentLoaded', callback)
    }

    Element.prototype.insertAfter = function(newNode) {
        this.parentNode.insertBefore(
            newNode,
            this.nextSibling
        );
    }

    Element.prototype.getOne = getOne
    Element.prototype.getAll = getAll

    /**
     * @param {String} action
     * @param {Function} callback
     * @return {void}
     */
    Array.prototype.addEventListener = function (action, callback) {
        this
            .filter(item => item instanceof HTMLElement)
            .forEach(
                element => element.addEventListener(
                    action,
                    () => callback(element)
                )
            )
    }

    /**
     * @param {any} value
     * @return {void}
     */
    Array.prototype.removeByValue = function (value) {
        while (this.indexOf(value) > -1) {
            this.splice(
                this.indexOf(
                    value
                ),
                1
            )
        }
    }

    /**
     * @param {...String} cssClasses
     * @return {void}
     */
    Array.prototype.removeClasses = function (...cssClasses) {
        this
            .filter(item => item instanceof HTMLElement)
            .forEach(element => element.classList.remove(...cssClasses))
    }

    /**
     * @param {...String} cssClasses
     * @return {void}
     */
    Array.prototype.addClasses = function (...cssClasses) {
        this
            .filter(item => item instanceof HTMLElement)
            .forEach(element => element.classList.add(...cssClasses))
    }

    /**
     * @param {String} query
     * @return {HTMLElement}
     */
    Document.prototype.getOne = getOne

    /**
     * @param {String} query
     * @return {HTMLElement[]}
     */
    Document.prototype.getAll = getAll
}